# kube_tensorboard

## Build image.
`docker build -t kube_tensorboard .`
`docker tag kube_tensorboard:latest gcr.io/serious-timer-256620/kube_tensorboard:latest`
`docker push gcr.io/serious-timer-256620/kube_tensorboard:latest`

## Install 
kubectl create namespace tensorboard
kubectl apply -f kube_tensorboard.yaml -n tensorboard
helm install tensorboard-nfs -f values-nfs-client.yaml -n tensorboard stable/nfs-client-provisioner

## Uninstall 
helm uninstall tensorboard-nfs -n tensorboard
kubectl delete -f kube_tensorboard.yaml -n tensorboard
kubectl delete namespace tensorboard

## Usage

Mount pvc `tensorboard-nfs` and write tensorboard logs to mounted volume.
Tensorboard available on service kube-tensorboard port 6006. Add to ingress or use kubectl port-forward to access service.


To clean up old tensorboard logs. Run 
`kubectl exec -it -n cartpole deployments/tensorboard /bin/bash`
Navigate to /exports and remove the old files.
